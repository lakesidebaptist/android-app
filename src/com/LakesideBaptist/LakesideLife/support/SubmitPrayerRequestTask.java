package com.LakesideBaptist.LakesideLife.support;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.widget.Toast;

import com.LakesideBaptist.LakesideLife.R;

public class SubmitPrayerRequestTask extends AsyncTask<HttpPost, Void, String> {

	private Context context = null;
	private ProgressDialog pd = null;

	public SubmitPrayerRequestTask(Context con) {
		context = con;
	}

	@SuppressWarnings("static-access")
	@Override
	protected void onPreExecute() {
		pd = new ProgressDialog(context,
				ProgressDialog.STYLE_SPINNER).show(
				context,
				context.getString(R.string.submitting),
				context.getString(R.string.submitting_prayer_request));
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(HttpPost... post) {
		HttpClient client = new DefaultHttpClient();
		HttpResponse response = null;

		try {
			response = client.execute(post[0]);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		BufferedReader reader = null;

		try {
			reader = new BufferedReader(new InputStreamReader(response
					.getEntity().getContent()));
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String responseString = null;
		try {
			responseString = reader.readLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return responseString;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		if (result.equals("Successful")) {
			pd.dismiss();
			Toast.makeText(context, context.getString(R.string.prayer_request_sucessful),
					Toast.LENGTH_SHORT).show();
		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setTitle(context.getString(R.string.prayer_request_error))
					.setCancelable(false)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									pd.dismiss();
									dialog.dismiss();
								}
							}).show();
		}
	}
}
