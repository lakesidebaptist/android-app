package com.LakesideBaptist.LakesideLife.support;

/**
 * This is the enumeration which correlates to the information one wishes to
 * obtain.
 */
public enum DataType {
	NEWS, SERMONS, DAILY_SCRIPTURE, SUNDAY_SCHOOL, MINISTERS, BLOGS, HTML_INFO
}