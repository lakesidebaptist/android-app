package com.LakesideBaptist.LakesideLife.support;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * @author andrew
 * 
 */
public class Checker {

	/**
	 * @return Checks if network connection is available and if so, starts the
	 *         updater
	 */
	public boolean hasConnection(Context context) {
		NetworkInfo ni = ((ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE))
				.getActiveNetworkInfo();

		if (ni != null) {
			return true;
		} else {
			return false;
		}
	}
}
