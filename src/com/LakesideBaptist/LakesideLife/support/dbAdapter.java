package com.LakesideBaptist.LakesideLife.support;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * The database adapter for the application database.
 * 
 * @author andrew
 */
public class dbAdapter {
	/**
	 * This should be changed whenever the format of the application database
	 * changes in such a way that the onUpgrade() method must be called.
	 */
	private static final int VERSION = 1;
	private DatabaseHelper dbHelper;
	private SQLiteDatabase db;
	private final Context context;

	public dbAdapter(Context context) {
		this.context = context;
		open();
	}

	/**
	 * Opens the database.
	 * 
	 * @return
	 */
	private dbAdapter open() {
		dbHelper = new DatabaseHelper(context);
		db = dbHelper.getWritableDatabase();
		return this;
	}

	public long insert(String table, ContentValues values) {
		return db.insert(table, null, values);
	}

	public void execSQL(String command) {
		db.execSQL(command);
	}

	/**
	 * Sumbits query to database. Note this function does NOT return anything.
	 * You must use the read() function to read from the database.
	 * 
	 * @param command
	 */
	public void rawQuery(String command) {
		db.rawQuery(command, null);
	}

	/**
	 * This function should be used to read from a database. Notice "table" is
	 * the only required parameter.
	 * 
	 * @param table
	 * @param what
	 * @param where
	 * @return
	 */
	public Cursor read(String table, String[] what, String where) {
		Cursor cur = db.query(table, what, where, null, null, null, null);
		return cur;
	}

	public Cursor rawRead(String cmd) {
		cmd = cmd.replace(";", "");
		Cursor cur = db.rawQuery(cmd, null);
		return cur;
	}

	/**
	 * Deletes a row from the database.
	 * 
	 * @param table
	 */
	public void deleteRow(String table, String whereName, String whereValue) {
		String command = "DELETE FROM '" + table + "' WHERE '" + whereName
				+ "'='" + whereValue + "';";
		rawQuery(command.replace("''", "'"));
	}

	public void dropTable(String table) {
		db.execSQL("DROP TABLE IF EXISTS'" + table + "'");
	}

	public String getSetting(String setting) {
		Cursor cursor = read("settings", new String[] { "value" }, "setting='"
				+ setting + "'");
		cursor.moveToFirst();
		String returnedValue = cursor.getString(cursor.getColumnIndex("value"));
		Log.i("get", returnedValue);
		cursor.close();

		return returnedValue;
	}

	public void changeSetting(String setting, String value) {
		ContentValues values = new ContentValues();
		values.put("value", value);
		db.update("settings", values, "setting='" + setting + "'", null);
	}

	/* To add a setting */
	public void addSetting(String setting, String value) {
		ContentValues cv = new ContentValues();
		cv.put("setting", setting);
		cv.put("value", value);
		insert("settings", cv);
	}

	/**
	 * Closes the database.
	 */
	public void close() {
		dbHelper.close();
	}

	/**
	 * I don't normally use nested classes due to my OCD, but I caved with this,
	 * because all the online examples did it, and I didn't have time to figure
	 * out how to make this class in its own file.
	 * 
	 * @author andrew
	 * 
	 */
	public class DatabaseHelper extends SQLiteOpenHelper {
		DatabaseHelper(Context context) {
			super(context, "lakeside", null, VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase database) {
			database.execSQL("CREATE TABLE 'settings' ('_id' integer primary key, 'setting' text, 'value' text);");
			String[][] settings = {
					{ "firstRun", "updateServiceTimout", "updateTimeUnit",
							"alert" }, { "true", "60", "minute", "none" } };
			ContentValues values = new ContentValues();

			for (int i = 0; i < settings[0].length; i++) {
				values.put("setting", settings[0][i]);
				values.put("value", settings[1][i]);
				database.insert("settings", null, values);
			}
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub

		}
	}
}