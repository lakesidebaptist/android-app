package com.LakesideBaptist.LakesideLife.support;

import java.util.ArrayList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.LakesideBaptist.LakesideLife.R;

public class ReselectAdapter extends ArrayAdapter<String> {
    private Context mContext;
    private ArrayList<String> mItems;
    private int mInternalPosition;

    public ReselectAdapter(Context context, ArrayList<String> items) {
        super(context, R.layout.sherlock_spinner_item, items);
        mContext = context;
        mItems = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = (TextView) convertView;
        if (textView == null) {
            textView = (TextView) LayoutInflater.from(mContext).inflate(
                    R.layout.sherlock_spinner_item, null);
            textView.setText(mItems.get(mInternalPosition));
        }
        return textView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup
            parent) {
        TextView textView = null;
        if (position == mItems.size() - 1) {
            textView = new TextView(mContext);
            textView.setHeight(0);
        } else {
            textView = (TextView) LayoutInflater.from(mContext)
                    .inflate(R.layout.sherlock_spinner_dropdown_item, 
                            parent, false);
            textView.setText(mItems.get(position));
        }
        parent.setVerticalScrollBarEnabled(false);
        return textView;
    }

    public void setInternalPosition(int internalPosition) {
        mInternalPosition = internalPosition;
    }
}