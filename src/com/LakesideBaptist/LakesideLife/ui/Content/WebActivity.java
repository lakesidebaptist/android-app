package com.LakesideBaptist.LakesideLife.ui.Content;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.LakesideBaptist.LakesideLife.R;
import com.LakesideBaptist.LakesideLife.support.Checker;
import com.LakesideBaptist.LakesideLife.ui.MainActivity;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;

public class WebActivity extends SherlockActivity {

	private ProgressDialog loadingDialog = null;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web);

		new ProgressDialog(this, ProgressDialog.STYLE_SPINNER);
		// Initializing the loading dialog
		loadingDialog = ProgressDialog.show(this,
				"", getString(R.string.loading_webpage));
		loadingDialog.setCancelable(true);
		loadingDialog.setCanceledOnTouchOutside(true);

		// Declaring the intent
		Intent intent = getIntent();
		String type = intent.getStringExtra("type");
		final String data = intent.getStringExtra("data");

		// Setting up the WebView
		final WebView wv = (WebView) findViewById(R.id.webview);

		// To disable clicking on links from webview
		wv.setWebViewClient(new WebViewClient() {

			// What to do when the page starts to load
			@Override
			public void onPageStarted(WebView w, String url, Bitmap favicon) {
				super.onPageStarted(wv, url, favicon);
				loadingDialog.show();
			}

			// What to do when the page is done loading
			@Override
			public void onPageFinished(WebView view, String url) {
				super.onPageFinished(wv, url);
				if (loadingDialog.isShowing()) {
					loadingDialog.dismiss();
				}
			}

			/* To make sure user doesn't surf the web through this activity */
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if (new Checker().hasConnection(MainActivity.context)) {
					if (url.contains("biblegateway.com")
							|| url.contains("lakesidebaptist.com")
							|| (url.contains("richard")
									&& url.contains("trader") || url
										.contains("hishandsingermany.com"))) {
						return false;
					}

					/*
					 * Starts a web browser if link clicked is not for church
					 * website or biblegateway.com or a blog
					 */
					startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
					return true;
				} else {
					return true;
				}
			}
		});

		// For loading HTML from a remote location
		if (type.equals("scripture") || type.equals("blog")) {
			setTitle(intent.getStringExtra("title"));
			WebSettings settings = wv.getSettings();
			settings.setJavaScriptEnabled(true);
			settings.setLoadsImagesAutomatically(true);
			settings.setSupportZoom(true);
			settings.setBuiltInZoomControls(true);
			wv.loadUrl(data);
		}

		if (type.equals("payload")) {
			ActionBar actionBar = getSupportActionBar();

			// Makes home button visible
			actionBar.setHomeButtonEnabled(true);

			// Allows home button to be used to navigate up
			actionBar.setDisplayHomeAsUpEnabled(true);
			
			setTitle(intent.getStringExtra("title"));
			WebSettings settings = wv.getSettings();
			settings.setJavaScriptEnabled(true);
			settings.setLoadsImagesAutomatically(true);
			settings.setSupportZoom(true);
			settings.setBuiltInZoomControls(true);
			wv.loadDataWithBaseURL("http://www.lakesidebaptist.com/", data,
					"text/html", "UTF-8", "about:blank");
		}
	}
}
