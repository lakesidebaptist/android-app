package com.LakesideBaptist.LakesideLife.ui.Content;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.LakesideBaptist.LakesideLife.R;
import com.LakesideBaptist.LakesideLife.support.dbAdapter;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockListActivity;

public class BlogActivity extends SherlockListActivity implements
		OnNavigationListener, DialogInterface.OnClickListener {
	private Cursor cursor = null;
	private dbAdapter dba = null;
	private String[] blogNameList = null;
	private String[] blogPathList = null;
	private String[] blogTitles = null;
	private SimpleCursorAdapter contentAdapter = null;
	private Context activityContext = this;
	private Context spinnerContext = null;
	private TextView blogName = null;
	private String[] FROM = new String[] { "title", "postdate" };
	private int[] TO = new int[] { android.R.id.text1, android.R.id.text2 };
	private String[] traderBlogsNames = null;
	private String[] traderBlogsUrl = null;
	private Intent intent = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_blog);

		ActionBar actionBar = getSupportActionBar();

		// Makes home button visible
		actionBar.setHomeButtonEnabled(true);

		// Allows home button to be used to navigate up
		actionBar.setDisplayHomeAsUpEnabled(true);

		// Initialize blogNameList
		blogNameList = getResources().getStringArray(R.array.blog_list_names);
		blogPathList = getResources().getStringArray(R.array.blog_list_paths);
		blogTitles = getResources().getStringArray(R.array.blog_title_names);

		traderBlogsNames = getResources().getStringArray(
				R.array.trader_blog_names);
		traderBlogsUrl = getResources().getStringArray(R.array.trader_blog_url);

		/*
		 * Initialize spinner's context to correct for this bug:
		 * https://github.com/JakeWharton/ActionBarSherlock/issues/268
		 */
		spinnerContext = actionBar.getThemedContext();

		setTitle(getString(R.string.blogs));

		blogName = ((TextView) findViewById(R.id.blog_name));

		// Creating the list
		ArrayAdapter<CharSequence> listAdapter = ArrayAdapter
				.createFromResource(spinnerContext, R.array.blog_list_names,
						R.layout.sherlock_spinner_item);
		listAdapter
				.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		actionBar.setListNavigationCallbacks(listAdapter, this);

		// Setting the first-listed blog to already be displayed
		dba = new dbAdapter(this);

		intent = new Intent(getApplicationContext(), WebActivity.class);
		intent.putExtra("type", "blog");
		
		setDisplayedBlog(1);
	}

	private void setDisplayedBlog(int blog) {
		blog -= 1;
		cursor = dba.read("blogs", null, "path='" + blogPathList[blog] + "'");
		contentAdapter = new SimpleCursorAdapter(this,
				android.R.layout.two_line_list_item, cursor, FROM, TO, 0);
		setListAdapter(contentAdapter);
		contentAdapter.notifyDataSetChanged();
		contentAdapter.setViewBinder(new ViewBinder() {

			@Override
			public boolean setViewValue(View view, Cursor cursor,
					int columnIndex) {
				if (view.getId() == android.R.id.text1) {
					((LinearLayout) view.getParent()).setTag(R.string.data_tag,
							cursor.getString(cursor.getColumnIndex("url")));
				}
				return false;
			}
		});

		blogName.setText(blogTitles[blog]);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		cursor.close();
		dba.close();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {

		if (itemPosition == 5) {
			AlertDialog.Builder builder = new AlertDialog.Builder(
					BlogActivity.this);
			builder.setTitle("Dr. Trader's Blogs").setIcon(R.drawable.trader)
					.setItems(R.array.trader_blog_names, this);
			builder.create().show();
		} else if (itemPosition == 6) {
			intent.putExtra("title", "His Hands In Germany");
			intent.putExtra("data", "http://www.hishandsingermany.com");
			intent.putExtra("type", "blog");
			startActivity(intent);
		} else if (itemPosition > 0) {
			setDisplayedBlog(itemPosition);
		}

		return true;
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		intent.putExtra("title", traderBlogsNames[which]);
		intent.putExtra("data", traderBlogsUrl[which]);
		intent.putExtra("type", "blog");
		startActivity(intent);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		intent.putExtra("title",
				((TextView) v.findViewById(android.R.id.text1)).getText());
		intent.putExtra("data", (String) v.getTag(R.string.data_tag));
		startActivity(intent);
	}
}