package com.LakesideBaptist.LakesideLife.ui.Content;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.LakesideBaptist.LakesideLife.R;
import com.LakesideBaptist.LakesideLife.support.dbAdapter;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockListActivity;

public class SermonActivity extends SherlockListActivity implements
		OnClickListener, ViewBinder {

	private Cursor cursor = null;
	private SimpleCursorAdapter adapter = null;
	private dbAdapter dba = null;
	private ListView listView = null;

	// Arrays
	private static String[] FROM = { "title", "scripture", "speaker", "mdy",
			"hour" }; // , "file", "extra"
	private static int[] TO = { R.id.sermon_title, R.id.sermon_scripture,
			R.id.sermon_speaker, R.id.sermon_date, R.id.sermon_time };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sermon);

		ActionBar actionBar = getSupportActionBar();

		// Makes home button visible
		actionBar.setHomeButtonEnabled(true);

		// Allows home button to be used to navigate up
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	protected void onDestroy() {
		super.onPause();
		dba.close();
		cursor.close();
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Setting up the database adapter
		dba = new dbAdapter(this);

		// Getting the Cursor of the requested data
		cursor = dba.rawRead("SELECT * FROM 'sermons'");
		adapter = new SimpleCursorAdapter(this, R.layout.sermon_list, cursor,
				FROM, TO, 0);
		adapter.setViewBinder(this);
		setListAdapter(adapter);
		setTitle(R.string.title_activity_sermon);
	}

	/*
	 * To dynamically change the time of day displayed as well as adding the
	 * title and file tags to the play buttons
	 */
	@Override
	public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
		int id = view.getId();

		// To set the sermon time
		if (id == R.id.sermon_time) {
			String time = cursor.getString(cursor.getColumnIndex("hour"));

			if (time.contains("AM")) {
				((TextView) view).setText("Sunday AM");
			} else if (time.contains("PM")) {
				((TextView) view).setText("Sunday PM");
			}

			/*
			 * Adding tags for the title and file name to the "Play audio/video"
			 * buttons
			 */
			RelativeLayout parent = (RelativeLayout) view.getParent();

			TextView playAudio = (TextView) parent
					.findViewById(R.id.sermon_audio);
			playAudio.setTag(R.string.data_tag,
					cursor.getString(cursor.getColumnIndex("file")));
			
			TextView playVideo = (TextView) parent
					.findViewById(R.id.sermon_video);
			playVideo.setTag(R.string.data_tag,
					cursor.getString(cursor.getColumnIndex("file")));

			return true;

		} else {
			return false;
		}
	}

	@Override
	public void onClick(View view) {
		int id = view.getId();

		// Starting WebActivity with needed data values
		if (id == R.id.sermon_audio || id == R.id.sermon_video) {

			Intent intent = new Intent(android.content.Intent.ACTION_VIEW);

			if (id == R.id.sermon_audio) {
				intent.setDataAndType(
						Uri.parse("http://audio.lakesidebaptist.com/"
								+ (String) view.getTag(R.string.data_tag)
								+ ".mp3"), "audio/mp3");
			} else if (id == R.id.sermon_video) {
				intent.setDataAndType(
						Uri.parse("http://video.lakesidebaptist.com/"
								+ (String) view.getTag(R.string.data_tag)
								+ ".mp4"), "video/mp4");
			}

			startActivity(intent);
		}
	}
}
