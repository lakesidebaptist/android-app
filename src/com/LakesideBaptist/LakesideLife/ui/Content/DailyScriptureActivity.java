package com.LakesideBaptist.LakesideLife.ui.Content;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.LakesideBaptist.LakesideLife.R;
import com.LakesideBaptist.LakesideLife.support.Checker;
import com.LakesideBaptist.LakesideLife.support.dbAdapter;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockListActivity;

public class DailyScriptureActivity extends SherlockListActivity implements
		OnItemClickListener {

	private Cursor cursor = null;
	private SimpleCursorAdapter adapter = null;
	private dbAdapter dba = null;
	private ListView listView = null;

	// Arrays
	private static String[] FROM = { "date", "scripture" };
	private static int[] TO = { R.id.daily_scripture_title,
			R.id.daily_scripture_body };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_daily_scripture);

		ActionBar actionBar = getSupportActionBar();

		// Makes home button visible
		actionBar.setHomeButtonEnabled(true);

		// Allows home button to be used to navigate up
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		cursor.close();
		dba.close();
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Setting up the database adapter
		dba = new dbAdapter(this);

		// Getting the Cursor of the requested data
		cursor = dba.read("daily_scripture", null, null);
		adapter = new SimpleCursorAdapter(this, R.layout.daily_scripture_list,
				cursor, FROM, TO, 0);
		setListAdapter(adapter);
		setTitle(R.string.title_activity_daily_scripture);

		listView = getListView();
		listView.setOnItemClickListener((OnItemClickListener) this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

		// Checking for an internet connection before executing code
		if (new Checker().hasConnection(this)) {

			// A cursor positioned at the list item that was clicked
			Cursor verseCursor = (Cursor) parent.getItemAtPosition(position);

			// Fetching the scripture clicked
			String passage = verseCursor.getString(verseCursor
					.getColumnIndex("scripture"));

			// Building intent & starting WebActivity
			Intent intent = new Intent(this, WebActivity.class);
			intent.putExtra("type", "scripture");
			intent.putExtra(
					"data",
					"http://mobile.biblegateway.com/passage/?search="
							+ passage.replace(" ", "%20"));
			intent.putExtra("title", passage);
			startActivity(intent);
		} else {

			// Informing user there is no internet connection
			Toast.makeText(getApplicationContext(), R.string.no_connection,
					Toast.LENGTH_SHORT).show();
		}
	}

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) { // Inflate the
	 * menu; this adds items to the action bar if it is present.
	 * getMenuInflater().inflate(R.menu.activity_daily_scripture, menu); return
	 * true; }
	 * 
	 * @Override public boolean onOptionsItemSelected(MenuItem item) { switch
	 * (item.getItemId()) { case android.R.id.home: // This ID represents the
	 * Home or Up button. In the case of this // activity, the Up button is
	 * shown. Use NavUtils to allow users // to navigate up one level in the
	 * application structure. For // more details, see the Navigation pattern on
	 * Android Design: // //
	 * http://developer.android.com/design/patterns/navigation.html#up-vs-back
	 * // NavUtils.navigateUpFromSameTask(this); return true; } return
	 * super.onOptionsItemSelected(item); }
	 */

}
