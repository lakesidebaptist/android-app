package com.LakesideBaptist.LakesideLife.ui.Content;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.LakesideBaptist.LakesideLife.R;
import com.LakesideBaptist.LakesideLife.support.Checker;
import com.LakesideBaptist.LakesideLife.support.SubmitPrayerRequestTask;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;

public class PrayerRequestActivity extends SherlockActivity {


	private ActionBar actionBar = null;
	private Context context = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_prayer_request);
		
		context = this;
		actionBar = getSupportActionBar();

		// Makes home button visible
		actionBar.setHomeButtonEnabled(true);

		// Allows home button to be used to navigate up
		actionBar.setDisplayHomeAsUpEnabled(true);

		((TextView) findViewById(R.id.requestBox))
				.setMovementMethod(new ScrollingMovementMethod());

		// Notify user of no internet connection
		if (!new Checker().hasConnection(this)) {
			noConnectionDialog();
		}

		// Collecting an assembling the data
		final Button button = (Button) findViewById(R.id.submitButton);
		button.setOnClickListener(new View.OnClickListener() {

			@SuppressWarnings("static-access")
			@Override
			public void onClick(View arg0) {

				String name = ((EditText) findViewById(R.id.nameBox)).getText()
						.toString();
				String email = ((EditText) findViewById(R.id.emailBox))
						.getText().toString();
				String member = "YES";
				boolean confidential = ((CheckBox) findViewById(R.id.confidentialBox))
						.isChecked();
				String request = ((EditText) findViewById(R.id.requestBox))
						.getText().toString();

				if (name == null || name == "") {
					name = "Anonymous";
				}

				if (email == null || email == "") {
					email = "none-provided";
				}

				if (!((CheckBox) findViewById(R.id.memberBox)).isChecked()) {
					member = "NO";
				}

				if (request == null || request == "") {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							PrayerRequestActivity.this);
					builder.setMessage(
							getString(R.string.no_prayer_request_entered))
							.setCancelable(false)
							.setPositiveButton("OK",
									new DialogInterface.OnClickListener() {

										@Override
										public void onClick(
												DialogInterface dialog,
												int which) {
											dialog.cancel();
										}
									}).show();
				} else {
					HttpPost post = new HttpPost(
							"http://lakesidebaptist.com/rss/iphone/iPrayPost.ashx");

					List<NameValuePair> nvp = new ArrayList<NameValuePair>(1);
					nvp.add(new BasicNameValuePair("em", email));
					nvp.add(new BasicNameValuePair("me", member));

					String submit = "FROM " + name + ", ";

					if (confidential) {
						submit += "CONFIDENTIAL: ";
					} else {
						submit += "NOT CONFIDENTIAL: ";
					}

					submit += request;
					nvp.add(new BasicNameValuePair("re", submit));

					try {
						post.setEntity(new UrlEncodedFormEntity(nvp));
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					if (!new Checker()
							.hasConnection(PrayerRequestActivity.this)) {
						noConnectionDialog();
					} else {
						new SubmitPrayerRequestTask(context).execute(post);
					}
				}
			}

		});
	}

	private void noConnectionDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.no_connection))
				.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				}).show();
	}
}
