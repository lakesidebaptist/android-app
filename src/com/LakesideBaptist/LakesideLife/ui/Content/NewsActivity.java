package com.LakesideBaptist.LakesideLife.ui.Content;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;

import com.LakesideBaptist.LakesideLife.R;
import com.LakesideBaptist.LakesideLife.support.dbAdapter;
import com.LakesideBaptist.LakesideLife.ui.MainActivity;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockListActivity;
import com.actionbarsherlock.view.MenuItem;

/**
 * The activity that shows all the news items.
 * 
 * @author andrew
 */
public class NewsActivity extends SherlockListActivity {

	private Cursor cursor = null;
	private SimpleCursorAdapter adapter = null;
	dbAdapter dba = null;

	// Arrays
	private static String[] FROM = {"title", "body"};
	private static int[] TO = {R.id.news_title, R.id.news_body};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_news);
		
		ActionBar actionBar = getSupportActionBar();

		// Makes home button visible
		actionBar.setHomeButtonEnabled(true);

		// Allows home button to be used to navigate up
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	protected void onResume() {
		super.onResume();
	
		//Setting up the database adapter
		dba = new dbAdapter(this);
		
		// Getting the Cursor of the requested data
		cursor = dba.read("news", null, null);
		adapter = new SimpleCursorAdapter(this, R.layout.news_list, cursor, FROM, TO, 0);
		setListAdapter(adapter);
		setTitle(R.string.news);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		cursor.close();
		dba.close();
	}

	/*
	 * This method is called when the user clicks the home button to go back. It
	 * stops this activity and returns the user to the home screen.
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = new Intent(this, MainActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		onStop();
		return true;
	}
}
