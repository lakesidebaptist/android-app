package com.LakesideBaptist.LakesideLife.ui.Content;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.LakesideBaptist.LakesideLife.R;
import com.LakesideBaptist.LakesideLife.support.dbAdapter;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockListActivity;

public class SundaySchoolActivity extends SherlockListActivity implements
		OnNavigationListener/* , ViewBinder */{
	private Cursor cursor = null;
	private dbAdapter dba = null;
	private SimpleCursorAdapter contentAdapter = null;
	private String[] FROM = new String[] { "title", "teachers", "room" };
	private int[] TO = new int[] { R.id.class_name, R.id.teachers,
			R.id.classroom };
	private String[] ssGroupList = null;
	private Context spinnerContext = null;
	private TextView ssGroupName = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Yes, activity_blog is correct.
		setContentView(R.layout.activity_blog);

		ActionBar actionBar = getSupportActionBar();

		// Makes home button visible
		actionBar.setHomeButtonEnabled(true);

		// Allows home button to be used to navigate up
		actionBar.setDisplayHomeAsUpEnabled(true);

		// Initialize blogNameList
		ssGroupList = getResources().getStringArray(
				R.array.sunday_school_groups);

		/*
		 * Initialize spinner's context to correct for this bug:
		 * https://github.com/JakeWharton/ActionBarSherlock/issues/268
		 */
		spinnerContext = actionBar.getThemedContext();

		setTitle(getString(R.string.sunday_school));

		// Yes, blog_name is correct
		ssGroupName = ((TextView) findViewById(R.id.blog_name));

		// Creating the list
		ArrayAdapter<CharSequence> listAdapter = ArrayAdapter
				.createFromResource(spinnerContext,
						R.array.sunday_school_groups,
						R.layout.sherlock_spinner_item);
		listAdapter
				.setDropDownViewResource(R.layout.sherlock_spinner_dropdown_item);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		actionBar.setListNavigationCallbacks(listAdapter, this);

		// Setting the first-listed blog to already be displayed
		dba = new dbAdapter(this);

		setDisplayedBlog(1);
	}

	private void setDisplayedBlog(int ssAgeGroup) {
		if (ssAgeGroup > 0) {
			cursor = dba.read("sunday_school", null, "age_group='" + ssGroupList[ssAgeGroup] + "'");

			if (!cursor.moveToFirst())
				Log.i("cursor", "no move to first");

			contentAdapter = new SimpleCursorAdapter(this,
					R.layout.sunday_school_list_item, cursor, FROM, TO, 0);
			setListAdapter(contentAdapter);
			// contentAdapter.setViewBinder(this);
			contentAdapter.notifyDataSetChanged();
			ssGroupName.setText(ssGroupList[ssAgeGroup]);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		cursor.close();
		dba.close();
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {

		if (itemPosition > 0) {
			setDisplayedBlog(itemPosition);
		}

		return true;
	}

	/*
	 * @Override public boolean setViewValue(View view, Cursor cursor, int
	 * columnIndex) { if (view.getId() == R.id.teachers) { ((TextView)
	 * view).setText("Teachers: " + ((TextView) view).getText()); return true; }
	 * else if (view.getId() == R.id.classroom) { ((TextView)
	 * view).setText("Room: " + ((TextView) view).getText()); return true; }
	 * else { return false; } }
	 */
}