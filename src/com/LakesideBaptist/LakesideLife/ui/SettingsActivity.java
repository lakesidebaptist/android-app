package com.LakesideBaptist.LakesideLife.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.LakesideBaptist.LakesideLife.R;
import com.LakesideBaptist.LakesideLife.support.dbAdapter;
import com.actionbarsherlock.app.SherlockActivity;

public class SettingsActivity extends SherlockActivity {

	dbAdapter dba = new dbAdapter(this);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		// Show the Up button in the action bar.
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		String time = dba.getSetting("updateServiceTimout");
		String unit = dba.getSetting("updateTimeUnit");
		
		if(time == null) {
			time = "60";
		}
		
		final EditText editInt = (EditText) findViewById(R.id.editInterval);		
		editInt.setText(time);
		
		final RadioButton minButton = (RadioButton) findViewById(R.id.minuteButton);
		final RadioButton hourButton = (RadioButton) findViewById(R.id.hourButton);
		
		if(unit == null || unit.equals("minute")) {
			minButton.setChecked(true);
			hourButton.setChecked(false);
		} else if(unit.equals("hour")) {
			minButton.setChecked(false);
			hourButton.setChecked(true);
		}
		
		Button saveButton = (Button) findViewById(R.id.saveButton);
		saveButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String intervalText = editInt.getText().toString();
				String unitType = null;
				
				if(minButton.isChecked()) {
					unitType = "minute";
				} else if (hourButton.isChecked()) {
					unitType = "hour";
				}
				
				dba.changeSetting("updateServiceTimeout", intervalText);
				dba.changeSetting("updateTimeUnit", unitType);
				finish();
			}
		});
	}
}
