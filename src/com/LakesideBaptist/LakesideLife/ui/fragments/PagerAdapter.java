package com.LakesideBaptist.LakesideLife.ui.fragments;

import java.util.Arrays;
import java.util.List;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.actionbarsherlock.app.SherlockListFragment;

/**
 * This class is sort-of the bridge between the MainActivity and the fragments
 * that go in the tabs.
 * 
 * @author andrew
 */
public class PagerAdapter extends FragmentPagerAdapter {

	public PagerAdapter(FragmentManager fm) {
		super(fm);
		// TODO Auto-generated constructor stub
	}

	private List<SherlockListFragment> fragList = Arrays.asList(
			new LifeFragment(), new MinistriesFragment(), new AboutFragment(),
			new ContactFragment());

	/* Returns the correct fragment for each tab. */
	@Override
	public Fragment getItem(int i) {
		return fragList.get(i);
	}

	// I'm not quite sure why this method is here.
	@Override
	public int getCount() {
		return 4;
	}
}