package com.LakesideBaptist.LakesideLife.ui.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.LakesideBaptist.LakesideLife.R;
import com.LakesideBaptist.LakesideLife.support.dbAdapter;
import com.LakesideBaptist.LakesideLife.ui.MainActivity;
import com.LakesideBaptist.LakesideLife.ui.Content.BlogActivity;
import com.LakesideBaptist.LakesideLife.ui.Content.SundaySchoolActivity;
import com.LakesideBaptist.LakesideLife.ui.Content.WebActivity;
import com.actionbarsherlock.app.SherlockListFragment;

public class AboutFragment extends SherlockListFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		/* Inflates view */
		View aboutView = inflater.inflate(R.layout.fragment_list, container,
				false);

		return aboutView;
	}

	@Override
	public void onResume() {

		/*
		 * The code below lists the options available under the "Life" tab. I'm
		 * not sure why, but these have to go in the onResume() method.
		 */

		/*
		 * Specifies the array in the resources to get the array of option names
		 * and the layout that the list is patterned after.
		 */
		setListAdapter(ArrayAdapter.createFromResource(MainActivity.context,
				R.array.aboutList, R.layout.fragment_list_item));

		// Creates the actual list view
		ListView listView = getListView();

		// Disables searching through the list
		listView.setTextFilterEnabled(false);

		// The method called when a list item is clicked
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// The intent to switch to
				Intent intent = null;
				dbAdapter dba = new dbAdapter(MainActivity.context);

				switch ((int) id) {

				case 0:
					intent = new Intent(MainActivity.context, WebActivity.class);

					Cursor serviceCursor = dba.read("html_info",
							new String[] { "html" }, "name='services'");
					serviceCursor.moveToFirst();
					String serviceHtml = serviceCursor.getString(serviceCursor
							.getColumnIndex("html"));
					serviceCursor.close();

					intent.putExtra("type", "payload");
					intent.putExtra("data", serviceHtml);
					break;

				case 1:
					intent = new Intent(MainActivity.context,
							SundaySchoolActivity.class);
					break;

				case 2:
					intent = new Intent(MainActivity.context, WebActivity.class);

					Cursor wednesdayCursor = dba.read("html_info",
							new String[] { "html" }, "name='wednesdays'");
					wednesdayCursor.moveToFirst();
					String wednesdayHtml = wednesdayCursor
							.getString(wednesdayCursor.getColumnIndex("html"));
					wednesdayCursor.close();

					intent.putExtra("type", "payload");
					intent.putExtra("data", wednesdayHtml);
					break;

				case 3:
					intent = new Intent(
							android.content.Intent.ACTION_VIEW,
							Uri.parse("google.navigation:q=Lakeside+Baptist+Church,+2865+Old+Rocky+Ridge+Road,+Birmingham,+AL+35243"));
					break;

				case 4:
					intent = new Intent(MainActivity.context,
							BlogActivity.class);
					break;

				case 5:
					intent = new Intent(MainActivity.context, WebActivity.class);

					Cursor feedCursor = dba.read("html_info",
							new String[] { "html" }, "name='feeds'");
					feedCursor.moveToFirst();
					String feedHtml = feedCursor.getString(feedCursor
							.getColumnIndex("html"));
					feedCursor.close();

					intent.putExtra("type", "payload");
					intent.putExtra("data", feedHtml);
					break;

				case 6:
					intent = new Intent(MainActivity.context, WebActivity.class);

					Cursor historyCursor = dba.read("html_info",
							new String[] { "html" }, "name='history'");
					historyCursor.moveToFirst();
					String historyHtml = historyCursor.getString(historyCursor
							.getColumnIndex("html"));
					historyCursor.close();

					intent.putExtra("type", "payload");
					intent.putExtra("data", historyHtml);
					break;
				}

				/*
				 * Adding the title to every intent, whether it is needed or
				 * not.
				 */
				intent.putExtra(
						"title",
						getResources().getStringArray(R.array.aboutList)[(int) id]);

				startActivity(intent);
				dba.close();
			}

		});

		super.onResume();
	}
}
