package com.LakesideBaptist.LakesideLife.ui.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.LakesideBaptist.LakesideLife.R;
import com.LakesideBaptist.LakesideLife.support.dbAdapter;
import com.LakesideBaptist.LakesideLife.ui.MainActivity;
import com.LakesideBaptist.LakesideLife.ui.Content.WebActivity;
import com.actionbarsherlock.app.SherlockListFragment;

public class MinistriesFragment extends SherlockListFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		/* Inflates view */
		View lifeView = inflater.inflate(R.layout.fragment_list, container,
				false);

		return lifeView;
	}

	/*
	 * The method below lists the options available under the "Ministries" tab.
	 */
	@Override
	public void onResume() {
		/*
		 * Specifies the array in the resources to get the array of option names
		 * and the layout that the list is patterned after.
		 */
		setListAdapter(ArrayAdapter.createFromResource(MainActivity.context,
				R.array.ministriesList, R.layout.fragment_list_item));

		// Creates the actual list view
		ListView listView = getListView();

		// Disables searching through the list
		listView.setTextFilterEnabled(false);

		// The method called when a list item is clicked
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// The intent to switch to
				Intent intent = new Intent(MainActivity.context,
						WebActivity.class);
				Cursor cursor = null;
				String where = null;
				dbAdapter dba = new dbAdapter(MainActivity.context);

				/*
				 * This switch helps generate the "where" statement for the DB
				 * query
				 */
				switch ((int) id) {

				case 0:
					where = "childhood";
					break;

				case 1:
					where = "royal-ambassadors";
					break;

				case 2:
					where = "upward";
					break;

				case 3:
					where = "high-school";
					break;

				case 4:
					where = "acteens";
					break;

				case 5:
					where = "MOPS";
					break;

				case 6:
					where = "senior-adults";
					break;

				case 7:
					where = "WMU";
					break;

				case 8:
					where = "recreation";
					break;

				case 9:
					where = "media-centers";
					break;
				}

				cursor = dba.read("html_info", new String[] { "html" },
						"name='" + where + "'");
				Log.i("db", Boolean.toString(cursor.moveToFirst()));
				intent.putExtra("type", "payload");
				intent.putExtra("data",
						cursor.getString(cursor.getColumnIndex("html")));
				intent.putExtra(
						"title",
						getResources().getStringArray(R.array.ministriesList)[(int) id]);
				startActivity(intent);
				cursor.close();
				dba.close();
			}

		});

		super.onResume();
	}
}
