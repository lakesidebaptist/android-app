package com.LakesideBaptist.LakesideLife.ui.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.LakesideBaptist.LakesideLife.R;
import com.LakesideBaptist.LakesideLife.support.dbAdapter;
import com.LakesideBaptist.LakesideLife.ui.MainActivity;
import com.LakesideBaptist.LakesideLife.ui.Content.DailyScriptureActivity;
import com.LakesideBaptist.LakesideLife.ui.Content.NewsActivity;
import com.LakesideBaptist.LakesideLife.ui.Content.PrayerRequestActivity;
import com.LakesideBaptist.LakesideLife.ui.Content.SermonActivity;
import com.LakesideBaptist.LakesideLife.ui.Content.WebActivity;
import com.actionbarsherlock.app.SherlockListFragment;

/**
 * This fragment lists the options under the "Life" tab according to the order
 * specified in the lifeList array (res/values/strings.xml).
 * 
 * @author andrew
 */
public class LifeFragment extends SherlockListFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		/* Inflates view */
		View lifeView = inflater.inflate(R.layout.fragment_list, container,
				false);

		return lifeView;
	}

	@Override
	public void onResume() {

		/*
		 * The code below lists the options available under the "Life" tab. I'm
		 * not sure why, but these have to go in the onResume() method.
		 */

		/*
		 * Specifies the array in the resources to get the array of option names
		 * and the layout that the list is patterned after.
		 */
		setListAdapter(ArrayAdapter.createFromResource(MainActivity.context,
				R.array.lifeList, R.layout.fragment_list_item));

		// Creates the actual list view
		ListView listView = getListView();

		// Disables searching through the list
		listView.setTextFilterEnabled(false);

		// The method called when a list item is clicked
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// The intent to switch to
				Intent intent = null;

				switch ((int) id) {

				case 0:
					intent = new Intent(MainActivity.context,
							NewsActivity.class);
					break;

				case 1:
					intent = new Intent(MainActivity.context,
							DailyScriptureActivity.class);
					break;

				case 2:
					intent = new Intent(MainActivity.context,
							PrayerRequestActivity.class);
					break;

				case 3:
					intent = new Intent(MainActivity.context,
							SermonActivity.class);
					break;
					
				case 4:
					intent = new Intent(MainActivity.context, WebActivity.class);
					intent.putExtra("type", "payload");
					
					// Getting data
					dbAdapter dba = new dbAdapter(MainActivity.context);
					Cursor cursor = dba.read("html_info", null, null);
					
					while(cursor.moveToNext()) {
						if(cursor.getString(cursor.getColumnIndex("name")).equals("Plan-Of-Salvation")) {
							break;
						}
					}
					
					intent.putExtra("data", cursor.getString(cursor.getColumnIndex("html")));
					intent.putExtra("title", "Plan Of Salvation");
					
					cursor.close();
					dba.close();
					
					break;
				}
				startActivity(intent);
			}

		});

		super.onResume();
	}
}
