package com.LakesideBaptist.LakesideLife.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.text.ClipboardManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.LakesideBaptist.LakesideLife.R;
import com.LakesideBaptist.LakesideLife.support.dbAdapter;
import com.LakesideBaptist.LakesideLife.ui.MainActivity;
import com.actionbarsherlock.app.SherlockListFragment;

/**
 * This fragment lists the options under the "Life" tab according to the order
 * specified in the lifeList array (res/values/strings.xml).
 * 
 * @author andrew
 */
@SuppressWarnings("deprecation")
public class ContactFragment extends SherlockListFragment implements
		ViewBinder, View.OnClickListener, View.OnLongClickListener {
	private dbAdapter dba = null;
	private Cursor cursor = null;
	private SimpleCursorAdapter adapter = null;
	private String[] FROM = { "minister", "position" };
	private int[] TO = { R.id.name, R.id.resp };

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		/* Inflates view */
		View lifeView = inflater.inflate(R.layout.contact_list, container,
				false);

		return lifeView;
	}

	@Override
	public void onResume() {

		/*
		 * The code below lists the options available under the "Contact" tab.
		 * I'm not sure why, but these have to go in the onResume() method.
		 */

		/*
		 * Specifies the array in the resources to get the array of option names
		 * and the layout that the list is patterned after.
		 */

		// Setting up the database adapter
		dba = new dbAdapter(MainActivity.context);

		// Getting the Cursor of the requested data
		cursor = dba.read("ministers", null, null);
		adapter = new SimpleCursorAdapter(MainActivity.context,
				R.layout.contact_list_item, cursor, FROM, TO, 0);
		adapter.setViewBinder(this);
		setListAdapter(adapter);

		Button callOffice = (Button) getView().findViewById(R.id.call_office);
		Button emailOffice = (Button) getView().findViewById(R.id.email_office);
		callOffice.setTag("2058221240");
		emailOffice.setTag("info@lakesidebaptist.com");
		callOffice.setOnClickListener(this);
		emailOffice.setOnClickListener(this);
		callOffice.setOnLongClickListener(this);
		emailOffice.setOnLongClickListener(this);
		
		super.onResume();
	}

	@Override
	public boolean setViewValue(View view, Cursor cur, int columnIndex) {
		switch (view.getId()) {
		case R.id.name:
			((TextView) view).setText(cur.getString(columnIndex).replace("'",
					""));
			RelativeLayout parent = (RelativeLayout) view.getParent();

			ImageView image = (ImageView) parent.findViewById(R.id.photo);
			byte[] byteArr = cur.getBlob(cur.getColumnIndex("photo"));
			image.setImageBitmap(BitmapFactory.decodeByteArray(byteArr, 0,
					byteArr.length));

			Button call = (Button) parent.findViewById(R.id.call_button);
			call.setTag(((String) cur.getString(cur.getColumnIndex("phone"))
					.replace("'", "")));
			call.setOnClickListener(this);
			call.setOnLongClickListener(this);

			Button email = (Button) parent.findViewById(R.id.email_button);
			email.setTag(((String) cur.getString(cur.getColumnIndex("email"))
					.replace("'", "")));
			email.setOnClickListener(this);
			email.setOnLongClickListener(this);

			break;
		case R.id.resp:
			((TextView) view).setText(cur.getString(columnIndex).replace("'",
					""));
			break;
		}
		return true;
	}

	@Override
	public void onClick(View view) {
		Intent intent = null;
		String data = (String) view.getTag();

		switch (view.getId()) {
		case R.id.call_office:
		case R.id.call_button:
			intent = new Intent(Intent.ACTION_CALL);
			intent.setData(Uri.parse("tel:" + data));
			break;

		case R.id.email_office:
		case R.id.email_button:
			intent = new Intent(Intent.ACTION_SEND);
			intent.putExtra(Intent.EXTRA_EMAIL, new String[] { data });
			intent.setType("message/rfc822");
			break;
		}

		startActivity(intent);
	}

	@Override
	public boolean onLongClick(View view) {
		ClipboardManager cbm = (ClipboardManager) MainActivity.context
				.getSystemService(Context.CLIPBOARD_SERVICE);
		cbm.setText((String) view.getTag());
		String thingCopied = null;
		int id = view.getId();

		if (id == R.id.call_button || id == R.id.call_office) {
			thingCopied = "Phone number";
		} else if (id == R.id.email_button || id == R.id.email_office) {
			thingCopied = "E-mail address";
		}

		Toast toast = Toast.makeText(getActivity().getApplicationContext(),
				thingCopied + " copied to clipboard.", Toast.LENGTH_SHORT);
		toast.show();
		return true;
	}

	@Override
	public void onDestroy() {
		dba.close();
		cursor.close();
		super.onStop();
	}
}
