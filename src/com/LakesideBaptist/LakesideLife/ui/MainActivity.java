package com.LakesideBaptist.LakesideLife.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;

import com.LakesideBaptist.LakesideLife.R;
import com.LakesideBaptist.LakesideLife.support.Checker;
import com.LakesideBaptist.LakesideLife.support.dbAdapter;
import com.LakesideBaptist.LakesideLife.ui.fragments.PagerAdapter;
import com.LakesideBaptist.LakesideLife.update.Updater;
import com.LakesideBaptist.LakesideLife.update.UpdaterTask;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.app.SherlockFragmentActivity;

public class MainActivity extends SherlockFragmentActivity implements
		ActionBar.TabListener {

	// The context used for the Updater class
	public static Context context;

	/*
	 * I'm not entirely sure what this does, but it's key in implementing the
	 * tab/swiping navigation. See documentation here:
	 * http://developer.android.com
	 * /reference/android/support/v4/view/ViewPager.html
	 */
	ViewPager viewPager;
	dbAdapter dba = null;

	// The AsyncTask class Updater
	public static Updater updater;

	private PagerAdapter pagerAdapter;

	@SuppressWarnings("static-access")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		context = getApplicationContext();

		dba = new dbAdapter(getApplicationContext());
		String alertMessage = dba.getSetting("alert");

		if (!alertMessage.equals("none")) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("URGENT MESSAGE FROM LAKESIDE")
					.setMessage(alertMessage)
					.setPositiveButton("OK",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									finish();
								}
							});
		}

		// Perform initial update
		if (dba.getSetting("firstRun").equals("true")) {

			/*
			 * Checks if network connection is available and if so, starts the
			 * updater
			 */

			if (new Checker().hasConnection(this)) {
				// Updates if there is a connection

				new UpdaterTask(this).execute();
				dba.changeSetting("firstRun", "false");

			} else {
				// Tells user there's no connection and closes activity
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle(getString(R.string.no_internet_dialog_title))
						.setMessage(getString(R.string.no_internet_dialog_body))
						.setCancelable(false)
						.setPositiveButton("OK",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										finish();
									}
								});
			}
		}

		dba.close();

		// Sets up tabs
		pagerAdapter = new PagerAdapter(getSupportFragmentManager());
		final ActionBar actionBar = getSupportActionBar();
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		viewPager = (ViewPager) findViewById(R.id.pager);
		viewPager.setAdapter(pagerAdapter);

		// The method is called when a tab is clicked or the user swipes.
		viewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						actionBar.setSelectedNavigationItem(position);
					}
				});

		// Creates tabs
		actionBar.addTab(actionBar.newTab().setText("Life")
				.setTabListener(this), true);
		actionBar.addTab(actionBar.newTab().setText("Ministries")
				.setTabListener(this));
		actionBar.addTab(actionBar.newTab().setText("About")
				.setTabListener(this));
		actionBar.addTab(actionBar.newTab().setText("Contact")
				.setTabListener(this));

	}

/*	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater menuInflater = getSupportMenuInflater();
		menuInflater.inflate(R.menu.activity_main, menu);
		return true;
	}*/

	/*
	 * @Override public boolean onOptionsItemSelected(MenuItem item) {
	 * 
	 * switch (item.getItemId()) { case R.id.menu_settings: Intent intent = new
	 * Intent(this, SettingsActivity.class); startActivity(intent); return true;
	 * 
	 * default: return super.onOptionsItemSelected(item); } }
	 */

	// Called when a tab is selected.
	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// TODO Auto-generated method stub

	}

}