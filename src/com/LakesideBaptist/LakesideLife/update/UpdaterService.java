package com.LakesideBaptist.LakesideLife.update;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.LakesideBaptist.LakesideLife.support.dbAdapter;
import com.LakesideBaptist.LakesideLife.ui.MainActivity;

public class UpdaterService extends Service {

	public static boolean serviceRunning = true;
	public static boolean currentlyUpdating = false;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	}

	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	/* To update the database every hour */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		new Thread() {
			public void run() {
				while (serviceRunning) {
/*					dbAdapter dba = new dbAdapter(MainActivity.context);
					
					String timeSetting = dba.getSetting("updateServiceTimout");
					long time;
					
					if(timeSetting != null) {
					time = Long.parseLong(timeSetting);
					} else {
						time = 1;
					}
					
					String unit = dba.getSetting("updateTimeUnit");
					
					if(unit == null || unit.equals("minute")) {
						time = time * 1000 * 60;
					} else if(unit.equals("hour")) {
						time = time * 1000 * 60 * 60;
					}*/
					
					try {
						//Thread.sleep(time);
						Thread.sleep(1000*60*24);
						currentlyUpdating = false;
					} catch (InterruptedException e) {
						serviceRunning = false;
						e.printStackTrace();
					}
					
					currentlyUpdating = true;
					new UpdaterTask(getApplicationContext()).execute();
					currentlyUpdating = false;
					
				//	dba.close();
				}
			}
		};

		return super.onStartCommand(intent, flags, startId);
	}
}
