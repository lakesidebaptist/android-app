package com.LakesideBaptist.LakesideLife.update;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.LakesideBaptist.LakesideLife.R;
import com.LakesideBaptist.LakesideLife.ui.MainActivity;
import com.google.android.gcm.GCMRegistrar;

public class UpdaterTask extends AsyncTask<Void, Void, String> {

	private Context context = null;
	private ProgressDialog pd = null;

	public UpdaterTask(Context con) {
		context = con;
	}

	@Override
	protected void onPreExecute() {
		pd = new ProgressDialog(MainActivity.context,
				ProgressDialog.STYLE_SPINNER).show(context,
				context.getString(R.string.progress_dialog_updating_title),
				context.getString(R.string.progress_dialog_updating_body));
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(Void... arg0) {
		// Push notification registration
		GCMRegistrar.checkDevice(context);
		GCMRegistrar.checkManifest(context);
		final String regId = GCMRegistrar.getRegistrationId(context);
		if (regId.equals("")) {
			GCMRegistrar.register(context, "444149233370");
		} else {
			Log.v("not.", "Already registered");
		}

		Updater updater = new Updater();

		if (!UpdaterService.currentlyUpdating) {
			updater.updateAll();
		}

		return null;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);
		pd.dismiss();
	}
}
