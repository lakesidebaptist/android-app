package com.LakesideBaptist.LakesideLife.update.fetcher.blogs;

import com.google.gson.annotations.SerializedName;

public class BlogsArticles {
	@SerializedName("title")
	public String title;
	
	@SerializedName("postdate")
	public String postdate;
	
	@SerializedName("id")
	public String id;
	
	@SerializedName("url")
	public String url;
}
