package com.LakesideBaptist.LakesideLife.update.fetcher.blogs;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class BlogsObject {
	@SerializedName("id")
	public String id;
	
	@SerializedName("path")
	public String path;
	
	@SerializedName("articles")
	public List<BlogsArticles> articles;
}
