package com.LakesideBaptist.LakesideLife.update.fetcher.blogs;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class BlogsList {
	@SerializedName("data")
	public List<BlogsObject> blogs;
}
