package com.LakesideBaptist.LakesideLife.update.fetcher.sunday_school;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class SundaySchoolGroups {
	@SerializedName("group")
	public String group;
	
	@SerializedName("classes")
	public List<SundaySchoolClasses> classes;
}
