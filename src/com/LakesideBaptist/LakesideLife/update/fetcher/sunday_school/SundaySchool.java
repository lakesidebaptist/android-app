package com.LakesideBaptist.LakesideLife.update.fetcher.sunday_school;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class SundaySchool {
	@SerializedName("data")
	public List<SundaySchoolGroups> sunday;
}
