package com.LakesideBaptist.LakesideLife.update.fetcher.sunday_school;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;

public class SundaySchoolClasses implements Parcelable {
	@SerializedName("title")
	public String title;

	@SerializedName("teachers")
	public String teachers;

	@SerializedName("callnum")
	public String callnum;

	@SerializedName("room")
	public String room;

	public SundaySchoolClasses(String title, String teachers, String callnum,
			String room) {
		this.title = title;
		this.teachers = teachers;
		this.callnum = callnum;
		this.room = room;
	}

	public SundaySchoolClasses(Parcel in) {
		String[] data = new String[4];

		in.readStringArray(data);
		this.title = data[0];
		this.teachers = data[1];
		this.callnum = data[2];
		this.room = data[3];

	}

	public int describeContents() {
		return 0;
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeStringArray(new String[] { this.title, this.teachers,
				this.callnum, this.room });

	}

	@SuppressWarnings("rawtypes")
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
		public SundaySchoolClasses createFromParcel(Parcel in) {
			return new SundaySchoolClasses(in);
		}

		public SundaySchoolClasses[] newArray(int size) {
			return new SundaySchoolClasses[size];
		}
	};

}
