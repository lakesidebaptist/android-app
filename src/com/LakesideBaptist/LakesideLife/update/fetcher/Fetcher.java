package com.LakesideBaptist.LakesideLife.update.fetcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import android.database.DatabaseUtils;

import com.LakesideBaptist.LakesideLife.update.fetcher.blogs.BlogsList;
import com.LakesideBaptist.LakesideLife.update.fetcher.blogs.BlogsObject;
import com.LakesideBaptist.LakesideLife.update.fetcher.daily_scripture.DailyScriptureList;
import com.LakesideBaptist.LakesideLife.update.fetcher.daily_scripture.DailyScriptureObject;
import com.LakesideBaptist.LakesideLife.update.fetcher.ministers.MinistersList;
import com.LakesideBaptist.LakesideLife.update.fetcher.ministers.MinistersObject;
import com.LakesideBaptist.LakesideLife.update.fetcher.news.NewsList;
import com.LakesideBaptist.LakesideLife.update.fetcher.news.NewsObject;
import com.LakesideBaptist.LakesideLife.update.fetcher.sermons.SermonsList;
import com.LakesideBaptist.LakesideLife.update.fetcher.sermons.SermonsObject;
import com.LakesideBaptist.LakesideLife.update.fetcher.sunday_school.SundaySchool;
import com.LakesideBaptist.LakesideLife.update.fetcher.sunday_school.SundaySchoolGroups;
import com.google.gson.Gson;

/**
 * This class generates the List objects of the requested information using the
 * Gson library.
 * 
 * @author andrew
 */
public class Fetcher {

	/**
	 * This method reads the HTML for certain pages on the website.
	 * 
	 * @param page
	 * @return
	 */
	public static String getHTML(String page) {
		return webToString(getData("page&page=" + page));
	}

	/**
	 * Gets appropriate JSON from the page specified and returns it as a
	 * BufferedReader object.
	 * 
	 * @return
	 */
	private static BufferedReader getData(String type) {
		URL url = null;
		BufferedReader reader = null;

		try {
			url = new URL(
					"http://lakesidebaptist.com/rss/iphone/json.ashx?option="
							+ type);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			reader = new BufferedReader(new InputStreamReader(url.openStream()));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return reader;
	}
	
	public static String webToString(BufferedReader reader) {
		StringBuilder strBuilder = new StringBuilder();
		String htmlData = "";
		
		try {
			while ((htmlData = reader.readLine()) != null) {
				strBuilder.append(htmlData);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return strBuilder.toString();
	}

	public static List<BlogsObject> blogs() {
		Gson gson = new Gson();
		BlogsList blogsList = gson.fromJson(getData("blogs"), BlogsList.class);
		List<BlogsObject> blogs = blogsList.blogs;
		return blogs;
	}

	public static List<DailyScriptureObject> dailyScripture() {
		Gson gson = new Gson();
		DailyScriptureList dailyScriptureList = gson.fromJson(getData("Bible"),
				DailyScriptureList.class);
		List<DailyScriptureObject> ds = dailyScriptureList.bible;
		return ds;
	}

	public static List<MinistersObject> ministers() {
		Gson gson = new Gson();
		MinistersList ministersList = gson.fromJson(getData("directory"),
				MinistersList.class);
		List<MinistersObject> ministers = ministersList.ministersList;
		return ministers;
	}

	public static List<NewsObject> news() {
		Gson gson = new Gson();
		NewsList newsList = gson.fromJson(getData("news"), NewsList.class);
		List<NewsObject> news = newsList.newsObj;
		return news;
	}

	public static List<SermonsObject> sermons() {
		Gson gson = new Gson();
		SermonsList sermonsList = gson.fromJson(getData("messages"),
				SermonsList.class);
		List<SermonsObject> sermons = sermonsList.messages;
		return sermons;
	}

	public static List<SundaySchoolGroups> sundaySchool() {
		Gson gson = new Gson();
		SundaySchool ss = gson.fromJson(getData("sunday"), SundaySchool.class);
		List<SundaySchoolGroups> ssg = ss.sunday;
		return ssg;
	}

}