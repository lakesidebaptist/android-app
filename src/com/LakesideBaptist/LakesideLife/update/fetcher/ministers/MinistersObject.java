package com.LakesideBaptist.LakesideLife.update.fetcher.ministers;

import com.google.gson.annotations.SerializedName;

public class MinistersObject {
	
	@SerializedName("email")
	public String email;
   	
	@SerializedName("minister")
	public String minister;
   	
	@SerializedName("phone")
	public String phone;
   	
	@SerializedName("photo")
	public String photo;
   	
	@SerializedName("position")
	public String position;
}
