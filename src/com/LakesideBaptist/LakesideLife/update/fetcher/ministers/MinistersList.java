package com.LakesideBaptist.LakesideLife.update.fetcher.ministers;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class MinistersList {
	@SerializedName("data")
	public List<MinistersObject> ministersList;
}
