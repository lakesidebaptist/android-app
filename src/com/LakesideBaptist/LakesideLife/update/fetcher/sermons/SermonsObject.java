package com.LakesideBaptist.LakesideLife.update.fetcher.sermons;

import com.google.gson.annotations.SerializedName;

public class SermonsObject {
	@SerializedName("title")
	public String title;
	
	@SerializedName("scripture")
	public String scripture;
	
	@SerializedName("speaker")
	public String speaker;
	
	@SerializedName("mdy")
	public String mdy;
	
	@SerializedName("hour")
	public String hour;
	
	@SerializedName("file")
	public String file;
}
