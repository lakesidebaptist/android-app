package com.LakesideBaptist.LakesideLife.update.fetcher.sermons;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class SermonsList {
	@SerializedName("data")
	public List<SermonsObject> messages;
}
