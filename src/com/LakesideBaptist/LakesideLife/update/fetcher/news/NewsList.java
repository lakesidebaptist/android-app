package com.LakesideBaptist.LakesideLife.update.fetcher.news;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class NewsList {
	@SerializedName("data")
	public List<NewsObject> newsObj;
}
