package com.LakesideBaptist.LakesideLife.update.fetcher.news;

import com.google.gson.annotations.SerializedName;

/**
 * @author andrew
 * The class that the GSON library uses to parse JSON strings of the news.
 */
public class NewsObject {
	@SerializedName("title")
	public String title;
	
	@SerializedName("body")
	public String body;
	
	@SerializedName("tags")
	public String tags;
}
