package com.LakesideBaptist.LakesideLife.update.fetcher.daily_scripture;

import com.google.gson.annotations.SerializedName;

public class DailyScriptureObject {
	@SerializedName("mdy")
	public String mdy;
	
	@SerializedName("scripture")
	public String scripture;
}
