package com.LakesideBaptist.LakesideLife.update.fetcher.daily_scripture;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class DailyScriptureList {
	@SerializedName("data")
	public List<DailyScriptureObject> bible;
}
