package com.LakesideBaptist.LakesideLife.update;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.IOUtils;

import android.content.ContentValues;
import android.database.DatabaseUtils;
import android.util.Log;

import com.LakesideBaptist.LakesideLife.support.DataType;
import com.LakesideBaptist.LakesideLife.support.dbAdapter;
import com.LakesideBaptist.LakesideLife.ui.MainActivity;
import com.LakesideBaptist.LakesideLife.update.fetcher.Fetcher;
import com.LakesideBaptist.LakesideLife.update.fetcher.blogs.BlogsArticles;
import com.LakesideBaptist.LakesideLife.update.fetcher.blogs.BlogsObject;
import com.LakesideBaptist.LakesideLife.update.fetcher.daily_scripture.DailyScriptureObject;
import com.LakesideBaptist.LakesideLife.update.fetcher.ministers.MinistersObject;
import com.LakesideBaptist.LakesideLife.update.fetcher.news.NewsObject;
import com.LakesideBaptist.LakesideLife.update.fetcher.sermons.SermonsObject;
import com.LakesideBaptist.LakesideLife.update.fetcher.sunday_school.SundaySchoolClasses;
import com.LakesideBaptist.LakesideLife.update.fetcher.sunday_school.SundaySchoolGroups;

/**
 * This class is an AsyncTask which updates the information in the application
 * database while allowing for specific information to be downloaded before
 * other information.
 * 
 * @author andrew
 * 
 */
public class Updater {

	// The database adapter.
	static private dbAdapter dba = null;

	/*
	 * A boolean array which records which data types have been updated already.
	 * The array index is the integer equivalent of the data type. NOTE:
	 * uninitialized (primitive) boolean variables are always false.
	 */
	static boolean updateStatus[] = new boolean[7];

	/*
	 * Updates all data.
	 */
	protected void updateAll() {

		// Opening database
		dba = new dbAdapter(MainActivity.context);

		// Iterates through data types, updating each one.
		for (DataType dt : DataType.values()) {
			dataTypeExecutor(dt);
		}

		// Closes the database.
		dba.close();
	}

	/* This method executes a method corresponding to a data type. */
	private static void dataTypeExecutor(DataType dt) {
		switch (dt) {
		case NEWS:
			news();
			break;
		case SERMONS:
			sermons();
			break;
		case SUNDAY_SCHOOL:
			sundaySchool();
			break;
		case DAILY_SCRIPTURE:
			dailyScripture();
			break;
		case BLOGS:
			blogs();
			break;
		case MINISTERS:
			ministers();
			break;
		case HTML_INFO:
			htmlInfo();
			break;
		}
	}

	/**
	 * This method acts as an alias for the sqlEscapeString(str) method in
	 * DatabaseUtils.
	 * 
	 * @param str
	 * @return
	 */
	private static String sant(String str) {
		String returnStr = null;

		try {
			returnStr = DatabaseUtils.sqlEscapeString(str);
		} catch (NullPointerException ex) {
			Log.e("SantitizationError!", "Trying to sanitize: " + str);
			returnStr = str;
		}

		return returnStr;
	}

	private static void blogs() {
		List<BlogsObject> blogsObject = Fetcher.blogs();
		Iterator<BlogsObject> boIt = blogsObject.iterator();
		String URL = null;
		BlogsObject bo = null;
		List<BlogsArticles> baList = null;
		Iterator<BlogsArticles> baIt = null;
		BlogsArticles ba = null;
		ContentValues values = null;

		Fetcher.getHTML("blogs");

		dba.dropTable("blogs");
		dba.execSQL("CREATE TABLE IF NOT EXISTS 'blogs' ('_id' integer primary key autoincrement,"
				+ "'path' text, 'title' text, 'postdate' text, 'url' text);");

		while (boIt.hasNext()) {
			bo = boIt.next();
			baList = bo.articles;
			baIt = baList.iterator();

			while (baIt.hasNext()) {
				ba = baIt.next();

				values = new ContentValues();
				values.put("path", bo.path);
				values.put("title", ba.title);
				values.put("postdate", ba.postdate);
				values.put("url", ba.url);

				dba.insert("blogs", values);
			}
		}

		updateStatus[DataType.BLOGS.ordinal()] = true;
	}

	private static void dailyScripture() {
		List<DailyScriptureObject> dailyScriptureList = Fetcher
				.dailyScripture();
		Iterator<DailyScriptureObject> it = dailyScriptureList.iterator();

		dba.dropTable("daily_scripture");
		dba.execSQL("CREATE TABLE 'daily_scripture' ('_id' integer primary key autoincrement,"
				+ "'date' text, 'scripture' text);");

		while (it.hasNext()) {
			DailyScriptureObject dsObj = it.next();

			String ufDate = dsObj.mdy;
			int monthInt = Integer.parseInt(ufDate.substring(0,
					ufDate.indexOf("/")));
			String day = ufDate.substring(ufDate.indexOf("/") + 1,
					ufDate.length());
			String finalDate = "";

			switch (monthInt) {
			case 1:
				finalDate = "January";
				break;
			case 2:
				finalDate = "February";
				break;
			case 3:
				finalDate = "March";
				break;
			case 4:
				finalDate = "April";
				break;
			case 5:
				finalDate = "May";
				break;
			case 6:
				finalDate = "June";
				break;
			case 7:
				finalDate = "July";
				break;
			case 8:
				finalDate = "August";
				break;
			case 9:
				finalDate = "September";
				break;
			case 10:
				finalDate = "October";
				break;
			case 11:
				finalDate = "November";
				break;
			case 12:
				finalDate = "December";
				break;
			}

			finalDate += " " + day;

			ContentValues values = new ContentValues();
			values.put("date", finalDate);
			values.put("scripture", dsObj.scripture);
			dba.insert("daily_scripture", values);
		}

		updateStatus[DataType.DAILY_SCRIPTURE.ordinal()] = true;
	}

	private static void htmlInfo() {
		String[] infoKeys = { "services", "wednesdays", "history",
				"directions", "Plan-Of-Salvation", "feeds", "childhood",
				"high-school", "senior-adults", "recreation", "upward",
				"media-centers", "MOPS", "discipleship", "WMU",
				"royal-ambassadors", "acteens" };

		dba.dropTable("html_info");
		dba.execSQL("CREATE TABLE 'html_info' ('name' text, 'html' text);");

		for (int index = 0; index < infoKeys.length; index++) {
			String name = infoKeys[index];
			String html = Fetcher.getHTML(infoKeys[index]);
			ContentValues values = new ContentValues();
			values.put("name", name);
			values.put("html", html);
			dba.insert("html_info", values);
		}

		updateStatus[DataType.HTML_INFO.ordinal()] = true;
	}

	private static void ministers() {
		List<MinistersObject> minObjList = Fetcher.ministers();
		Iterator<MinistersObject> it = minObjList.iterator();

		dba.dropTable("ministers");
		dba.execSQL("CREATE TABLE 'ministers' ('_id' integer primary key autoincrement,"
				+ "'minister' text, 'position' text, 'email' text, 'phone' text, 'photo' BLOB);");

		while (it.hasNext()) {
			MinistersObject minObj = it.next();

			URL url = null;

			try {
				url = new URL("http://www.lakesidebaptist.com/images/pgi/"
						+ minObj.photo);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			InputStream is = null;

			try {
				is = url.openConnection().getInputStream();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			byte[] ba = null;

			try {
				ba = IOUtils.toByteArray(is);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			ContentValues values = new ContentValues();
			values.put("minister", sant(minObj.minister));
			values.put("position", sant(minObj.position));
			values.put("email", sant(minObj.email + "@lakesidebaptist.com"));
			values.put("phone", sant(minObj.phone.replace(".", "")));
			values.put("photo", ba);
			dba.insert("ministers", values);
		}

		updateStatus[DataType.MINISTERS.ordinal()] = true;
	}

	private static void news() {
		List<NewsObject> newsList = Fetcher.news();
		Iterator<NewsObject> it = newsList.iterator();

		dba.execSQL("DROP TABLE IF EXISTS 'news';");
		dba.execSQL("CREATE TABLE IF NOT EXISTS 'news' ('_id' INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ "'title' text, 'body' text, 'tags' text);");
		// TODO blogs and news are the only thing that need the push
		// notifications
		while (it.hasNext()) {
			NewsObject newsObj = (NewsObject) it.next();
			ContentValues values = new ContentValues();
			values.put("title", newsObj.title);
			values.put("body", newsObj.body);
			values.put("tags", newsObj.tags);
			dba.insert("news", values);
		}

		updateStatus[DataType.NEWS.ordinal()] = true;
	}

	private static void sermons() {
		List<SermonsObject> sermonsList = Fetcher.sermons();
		Iterator<SermonsObject> it = sermonsList.iterator();

		dba.dropTable("sermons");
		dba.execSQL("CREATE TABLE 'sermons' ('_id' integer primary key autoincrement,"
				+ "'title' text, 'scripture' text, 'speaker' text, 'mdy' text, 'hour' text, 'file' text);");

		while (it.hasNext()) {
			SermonsObject sermonsObj = it.next();

			ContentValues values = new ContentValues();
			values.put("title", sermonsObj.title);
			values.put("scripture", sermonsObj.scripture);
			values.put("speaker", sermonsObj.speaker);
			values.put("mdy", sermonsObj.mdy);
			values.put("hour", sermonsObj.hour);
			values.put("file", sermonsObj.file);

			dba.insert("sermons", values);
		}

		updateStatus[DataType.SERMONS.ordinal()] = true;
	}

	private static void sundaySchool() {
		List<SundaySchoolGroups> ssgList = Fetcher.sundaySchool();
		Iterator<SundaySchoolGroups> ssgIt = ssgList.iterator();

		dba.dropTable("sunday_school");
		dba.execSQL("CREATE TABLE 'sunday_school' ('_id' integer primary key autoincrement, "
				+ "'age_group' text, 'title' text, 'teachers' text, 'room' text);");

		while (ssgIt.hasNext()) {
			SundaySchoolGroups ssg = ssgIt.next();
			List<SundaySchoolClasses> sscList = ssg.classes;
			Iterator<SundaySchoolClasses> sscIt = sscList.iterator();
			String group = ssg.group;

			if (group.contains("Adult")) {
				group = "Adult";
			}

			while (sscIt.hasNext()) {
				SundaySchoolClasses ssc = sscIt.next();

				// Because room numbers are sometimes null...
				String room = ssc.room;

				if (room == null) {
					room = "not listed";
				}

				ContentValues values = new ContentValues();
				values.put("age_group", sant(group).replace("'", ""));
				values.put("title", sant(ssc.title).replace("'", ""));
				values.put("teachers", sant(ssc.teachers).replace("'", ""));
				values.put("room", sant(room).replace("'", ""));
				dba.insert("sunday_school", values);
			}
		}

		updateStatus[DataType.SUNDAY_SCHOOL.ordinal()] = true;
	}
}